﻿using ProcessInfo.NET;
string input;
int pid;
START:
Console.WriteLine("input pid");
input = Console.ReadLine();
bool ret = int.TryParse(input, out pid);
if (!ret)
{
    Console.WriteLine("input error");
    goto START;
}

var info = ProcessEx.GetProcessInfo(pid);
System.Console.WriteLine($"{info.cpu}   {info.mempercent}   {info.mem}");

System.Console.WriteLine("over");
Console.ReadLine();
