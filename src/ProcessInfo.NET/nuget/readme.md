## 库功能

获取进程信息：

> * cpu 利用率
> * 内存 占用率
> * 内存 占用量

## 实现

借助：go 语言的 [gopsutil: psutil for golang (github.com)](https://github.com/shirou/gopsutil) <mark>v3</mark>库与 cgo 导出部分方法实现。

## 示例

```c#
using ProcessInfo.NET;
string input;
int pid;
START:
Console.WriteLine("input pid");
input = Console.ReadLine();
bool ret = int.TryParse(input, out pid);
if (!ret)
{
    Console.WriteLine("input error");
    goto START;
}

var info = ProcessEx.GetProcessInfo(pid);
System.Console.WriteLine($"{info.cpu}   {info.mempercent}   {info.mem}");

System.Console.WriteLine("over");
Console.ReadLine();
```

## 库补充

该库可结合[whuanle/CZGL.SystemInfo: 一个.NET Core监控系统CPU内存等信息的工具 (github.com)](https://github.com/whuanle/CZGL.SystemInfo)使用。



