﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace ProcessInfo.NET.Sdk
{
    [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential)]
    public struct ProcessInfo
    {
        /// double
        public double cpu;

        /// double
        public double mempercent;

        /// double
        public double mem;
    }
}
