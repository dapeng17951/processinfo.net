﻿namespace ProcessInfo.NET.Sdk
{
    internal static class NativeProcessInfo
    {
        /// Return Type: ProcessInfo
        ///pid: int
        [System.Runtime.InteropServices.DllImport(ProcessInfoLibrary.DllName, EntryPoint = "GetProcessInfo")]
        public static extern ProcessInfo GetProcessInfo(int pid);
    }
}
