﻿using ProcessInfo.NET.Sdk;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessInfo.NET
{
    public abstract class ProcessEx
    {

        public static ProcessInfo.NET.Sdk.ProcessInfo GetProcessInfo(int pid)
        {
            return NativeProcessInfo.GetProcessInfo(pid);
        }
    }
}
