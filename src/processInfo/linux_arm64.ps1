$env:CGO_ENABLED = "1"
$env:GOOS = "linux"
$env:GOARCH = "arm64"
$env:CC="zig cc -target aarch64-linux-gnu"
$env:CXX="zig c++ -target aarch64-linux-gnu"

go build -ldflags "-s -w" -buildmode=c-shared -o libProcessInfo_aarch64.so ./main.go