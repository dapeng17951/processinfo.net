$env:CGO_ENABLED = "1"
$env:GOOS = "windows"
$env:GOARCH = "386"
$env:CC="zig cc -target x86-windows-gnu"
$env:CXX="zig c++ -target x86-windows-gnu"

go build -ldflags "-s -w" -buildmode=c-shared -o ProcessInfo_386.dll ./main.go