$env:CGO_ENABLED = "1"
$env:GOOS = "linux"
$env:GOARCH = "amd64"
$env:CC="zig cc -target x86_64-linux-gnu"
$env:CXX="zig c++ -target x86_64-linux-gnu"

go build -ldflags "-s -w" -buildmode=c-shared -o libProcessInfo_x86_64.so ./main.go