package main

/*
#cgo CFLAGS: -I.
#include "ProcessInfo.h"
*/
import "C"
import (
	_ "unsafe"

	"github.com/shirou/gopsutil/v3/process"
)

//export GetProcessInfo
func GetProcessInfo(pid int32) C.struct_ProcessInfo {
	pinfo := C.struct_ProcessInfo{0, 0, 0}

	p, err := process.NewProcess(pid)
	if err != nil {
		return pinfo
	}
	if cpu, err := p.CPUPercent(); err == nil {
		pinfo.cpu = C.double(cpu)
	}
	if mem, err := p.MemoryPercent(); err == nil {
		pinfo.mempercent = C.double(float64(mem))
	}
	if memInfo, err := p.MemoryInfo(); err == nil {
		pinfo.mem = C.double(float64(memInfo.RSS) / 1024 / 1024)
	}
	return pinfo
}

func main() {

}
