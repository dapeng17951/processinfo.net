$env:CGO_ENABLED = "1"
$env:GOOS = "linux"
$env:GOARCH = "arm"
$env:CC="zig cc -target arm-linux-gnueabi"
$env:CXX="zig c++ -target arm-linux-gnueabi"

go build -ldflags "-s -w" -buildmode=c-shared -o libProcessInfo_armv6.so ./main.go