$env:CGO_ENABLED = "1"
$env:GOOS = "linux"
$env:GOARCH = "386"
$env:CC="zig cc -target x86-linux-gnu"
$env:CXX="zig c++ -target x86-linux-gnu"

go build -ldflags "-s -w" -buildmode=c-shared -o libProcessInfo_386.so ./main.go