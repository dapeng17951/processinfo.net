# 获取脚本所在目录
$scriptsFolder = "./"

# 获取目录下所有的.ps1文件
$scripts = Get-ChildItem -Path $scriptsFolder -Filter "*.ps1" -File

foreach ($script in $scripts) {
    # 执行每个脚本
    & $script.FullName
}