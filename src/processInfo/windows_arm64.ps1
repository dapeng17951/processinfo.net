$env:CGO_ENABLED = "1"
$env:GOOS = "windows"
$env:GOARCH = "aarch64"
$env:CC="zig cc -target aarch64-windows-msvc"
$env:CXX="zig c++ -target aarch64-windows-msvc"

go build -ldflags "-s -w" -buildmode=c-shared -o ProcessInfo_aarch64.dll ./main.go