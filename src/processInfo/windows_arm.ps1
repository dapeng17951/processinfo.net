$env:CGO_ENABLED = "1"
$env:GOOS = "windows"
$env:GOARCH = "arm"
$env:CC="zig cc -target arm-windows-msvc"
$env:CXX="zig c++ -target arm-windows-msvc"

go build -ldflags "-s -w" -buildmode=c-shared -o ProcessInfo_arm.dll ./main.go