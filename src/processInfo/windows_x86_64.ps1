$env:CGO_ENABLED = "1"
$env:GOOS = "windows"
$env:GOARCH = "amd64"
$env:CC="zig cc -target x86_64-windows-gnu"
$env:CXX="zig c++ -target x86_64-windows-gnu"

go build -ldflags "-s -w" -buildmode=c-shared -o ProcessInfo_x86_64.dll ./main.go